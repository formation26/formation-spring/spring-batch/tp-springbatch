package com.example.TP1_SpringBatch.jobconfiguration;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;

import java.io.File;

public class StepExecutionListener implements org.springframework.batch.core.StepExecutionListener {
    @Override
    public void beforeStep(StepExecution stepExecution) {

    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        System.out.println(" MonStep exécuté en " +stepExecution.getStepName());
        File file=new File("products.csv");
        file.setWritable(true);
        return null;
    }
}
