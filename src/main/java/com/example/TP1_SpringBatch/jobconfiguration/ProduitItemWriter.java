package com.example.TP1_SpringBatch.jobconfiguration;

import com.example.TP1_SpringBatch.Message.ProduitMessage;
import com.example.TP1_SpringBatch.dto.ProduitDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.jms.JmsItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsOperations;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
public class ProduitItemWriter implements ItemWriter<ProduitMessage> {
Logger logger= LoggerFactory.getLogger(ProduitItemWriter.class);
   
    JmsTemplate jmsTemplate ;
    public ProduitItemWriter(JmsTemplate jmsTemplate){
        this.jmsTemplate=jmsTemplate;
        logger.info("initialisation du ProduitItemWriter");

    }
    int counter =0;
    @Override
    public void write(List<? extends ProduitMessage> list) throws Exception {

        //SpringBatch
        /* JmsItemWriter<ProduitDto> jmsItemWriter=new JmsItemWriter<>();
        jmsItemWriter.setJmsTemplate(jmsTemplate);
        jmsItemWriter.write(list);*/

        //ActiveMQ - Pas JmsItemWriter


        for(ProduitMessage message:list){
            logger.info("Message sent");
            jmsTemplate.send("Batch_Product",message);
            logger.debug("Message sent");

        }

    }
}
