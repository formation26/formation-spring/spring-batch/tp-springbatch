package com.example.TP1_SpringBatch.jobconfiguration;

import com.example.TP1_SpringBatch.dto.ProduitDto;
import com.opencsv.CSVReader;

import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import java.io.FileReader;
import java.io.IOException;


public class ProduitItemReader implements ItemReader<ProduitDto> {

    private String filePath;
    CSVReader csvReader;

    String[] nextLines;

    //Constructeur qui permet d'initialiser le CSVReader au moment de la construction de ProduitItemReader
    public ProduitItemReader(String filePath) throws IOException {
        this.filePath=filePath;
        csvReader=new CSVReader(new FileReader(filePath));
        csvReader
                .skip(1);//Permet de ne pas prendre en compte le Header du fichier CSV
    }

    //Méthode de ItemReader
    @Override
    public ProduitDto read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {

        nextLines=csvReader
                .readNext(); //Ligne entière   champ1,champ2,champ3... à la sortie on a une tableau de chaînes {"champ1","champs2"...}
        if(nextLines==null){
            return null;
        }
        String name=nextLines[0];
        double price=Double.parseDouble(nextLines[1]);
        String description=nextLines[2];
        String photoUrl=nextLines[3];

        return new ProduitDto(name,price,description,photoUrl);
    }
}
