package com.example.TP1_SpringBatch.jobconfiguration;

import org.springframework.batch.core.configuration.annotation.DefaultBatchConfigurer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

@Configuration
public class DatabaseConfig  {

    @Bean
    @Primary
    @Qualifier("product-database")
    @ConfigurationProperties(prefix="spring.datasource.metier")
    public DataSource datasource() {
        return DataSourceBuilder.create().build();
    }


    @Bean
    @Qualifier("batch-database")
    @ConfigurationProperties(prefix="spring.datasource.batch")
    public DataSource batchDatasource() {
        return DataSourceBuilder.create().build();
    }
}
