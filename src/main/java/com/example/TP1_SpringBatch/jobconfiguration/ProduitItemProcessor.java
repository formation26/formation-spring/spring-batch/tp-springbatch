package com.example.TP1_SpringBatch.jobconfiguration;

import com.example.TP1_SpringBatch.Message.ProduitMessage;
import com.example.TP1_SpringBatch.dto.ProduitDto;
import org.springframework.batch.item.ItemProcessor;

public class ProduitItemProcessor implements ItemProcessor<ProduitDto, ProduitMessage> {
    @Override
    public ProduitMessage process(ProduitDto produitDto) throws Exception {
        return new ProduitMessage(produitDto.toString());
    }
}
