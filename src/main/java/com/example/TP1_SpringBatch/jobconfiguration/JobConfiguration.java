package com.example.TP1_SpringBatch.jobconfiguration;

import com.example.TP1_SpringBatch.Message.ProduitMessage;
import com.example.TP1_SpringBatch.dto.ProduitDto;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.configuration.annotation.DefaultBatchConfigurer;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.core.JmsTemplate;

import javax.sql.DataSource;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

@Configuration
@EnableBatchProcessing
public class JobConfiguration extends DefaultBatchConfigurer {
     /*@Autowired
     @Qualifier("batchDatasource")
     DataSource batchDatasource;*/

    @Override
    @Autowired
    public void setDataSource(@Qualifier("batch-database") DataSource batchDataSource) {
        super.setDataSource(batchDataSource);
    }
    /*@Autowired
    StepBuilderFactory stepBuilderFactory;*/
    StepBuilderFactory stepBuilderFactory;

    //Step
 public JobConfiguration(StepBuilderFactory stepBuilderFactory){
     this.stepBuilderFactory = stepBuilderFactory;
 }
    @Bean
    public Step monStep(){
        return stepBuilderFactory.get("step1")
                .tasklet(new MyTasklet())
                .tasklet(new MyTasklet())
                .build();
    }

    @Bean
    public Step monStep2(){
        return stepBuilderFactory.get("step2").tasklet(
                (StepContribution stepContribution, ChunkContext chunkContext)-> {
            System.out.println("Je suis step 2");
            return RepeatStatus.FINISHED;
        }).build();
    }

    @Bean
    public ItemWriter<ProduitMessage> produitItemWriter(JmsTemplate jmsTemplate){
           return new ProduitItemWriter(jmsTemplate);
    }

    @Bean
    public ItemReader<ProduitDto> csvItemreader(){

            try {
                File file=new File("products.csv");
                if(file.exists()){
                    file.setReadOnly();
                   return new ProduitItemReader("products.csv");
               }
                else{
                   throw new FileNotFoundException("le fichier source n'existe pas");
                }
            }
            catch (IOException e) {
                throw new RuntimeException("erreur d'entrée sortie");

        }
    }

    @Bean
    public ItemProcessor<ProduitDto,ProduitMessage> convertProduitDtoToProduitMessageProcessor(){
     return new ProduitItemProcessor();
    }

    @Bean
    public StepExecutionListener timeCounterStep(){
     return new com.example.TP1_SpringBatch.jobconfiguration.StepExecutionListener();
    }

    @Bean
    public Step monStep3( JmsTemplate jmsTemplate) throws FileNotFoundException, IOException {
        return stepBuilderFactory.get("step3")
                .<ProduitDto, ProduitMessage> chunk(100)
                .reader(csvItemreader())
                .processor(convertProduitDtoToProduitMessageProcessor())
                .writer(produitItemWriter(jmsTemplate))
                .listener(timeCounterStep())
                .build();
    }

    //Job
    @Bean
    public Job monJob(JobBuilderFactory jobBuilderFactory, JmsTemplate jmsOperations) throws FileNotFoundException, IOException{
        return jobBuilderFactory.get("mon-job")
                .start(monStep3(jmsOperations))
                .next(monStep())
                .build();
    }
}
