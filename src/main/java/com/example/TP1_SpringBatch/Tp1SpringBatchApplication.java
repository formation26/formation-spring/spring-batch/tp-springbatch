package com.example.TP1_SpringBatch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tp1SpringBatchApplication {

	public static void main(String[] args) {
		SpringApplication.run(Tp1SpringBatchApplication.class, args);
	}

}
