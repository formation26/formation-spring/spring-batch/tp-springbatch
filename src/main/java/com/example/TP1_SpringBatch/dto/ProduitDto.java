package com.example.TP1_SpringBatch.dto;

public class ProduitDto {

    private String nom;
    private double prixHT;
    private String description;
    private String photoUrl;

    public ProduitDto(String nom, double prixHT, String description, String photoUrl) {
        this.nom = nom;
        this.prixHT = prixHT;
        this.description = description;
        this.photoUrl = photoUrl;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public double getPrixHT() {
        return prixHT;
    }

    public void setPrixHT(double prixHT) {
        this.prixHT = prixHT;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    @Override
    public String toString() {
        return nom+"|"+prixHT+"|"+description+"|"+photoUrl;
    }
}
