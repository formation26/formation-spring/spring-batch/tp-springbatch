package com.example.TP1_SpringBatch.Message;

import org.springframework.jms.core.MessageCreator;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

public class ProduitMessage implements MessageCreator {

    private String message=null;

    public ProduitMessage(String messageContent){
        this.message=messageContent;
    }
    @Override
    public Message createMessage(Session session) throws JMSException {

        return session.createTextMessage(message);
    }
}
